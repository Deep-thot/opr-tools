# OPR Tools
Userscript for Ingress Operation Portal Recon - https://opr.ingress.com/recon

![](./image/opr-tools.png)

Features:
- Additional links to map services like Intel, OpenStreetMap, bing and some german ones
- Disabled annoying automatic page scrolling
- Removed "Your analysis has been recorded." dialog

Download: https://gitlab.com/1110101/opr-tools/raw/master/opr-tools.user.js